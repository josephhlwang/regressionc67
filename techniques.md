# Linear Regression Techniques

Notes to a successful linear regression model.

### 1. Data Adjustment

- first step after data collection is to check the data is even usable.

- proceed to clean the data, get rid of any null data and extreme outliers.

Use: DFFITS, Cook's Distance, or DFBETAS outlier detection.

### 2. Analyzing Covariates

After data is clean:

- visualize pairwise relationships between the response variable and the predictor variables.
- calculate the correlation matrix.
- plot histograms of the response variable and possible correlated covariates to visualize distributions.

Use: pairwise plots/correlation matrix, histograms

### 3. Find Initial Model

- pick correlated covariates and fit a full model with all interaction terms and run backward stepAIC.
- resulting model should contain the optimal subset of covariates and interaction terms based on AIC.

Also:

- check adjR^2 for model fit with adjustment on the number of variables.
- check model p-val for model significance.
- check p-val of individual variables for variable significance.

Use: stepAIC, summary

### 4. Check Model Assumptions

A model can have significance and a decent adjR^2 but failure to satisfy model assumptions makes it a worthless linear model(another type of model can be used).

- check fitted-values vs residuals for constant variance and linearity on 0.
- check QQplot and shapiro-welk for Normality.
- use the boxcox transformation to boost model performance

Repeat from step 3 with different initial "full models" and select "optimal model" based on adjR^2 and p-val.

Use: qqplot, residual vs fitted plot, shapiro-welk test, boxcox transformation

### 5. Train and Validation

Now knowing the formula(parameters) to the best model with transformation:

- split the data 30% validation, 70% training.
- fit a new linear model with the "optimal" formula on the training set.
- fit a new linear model with the "optimal" formula on the validation set.
- validate this is a good model by MSPR(training) == MSE(validation)

Repeat model selection process if there are any irregularities in the validation process.

Use: anova

### 6. Analysis

Given the model from training set analyze the significance

- variable p-vals tell us significance of individual variable within current model.
  - run tests on insignificant variables.
- explain beta values and their significance.
  - slope vs. intercept for interaction terms etc.
- note any weird beta values
  - eg. positive correlation but negative beta value
- fit and residuals
  - analysis of assumptions
- check outliers
